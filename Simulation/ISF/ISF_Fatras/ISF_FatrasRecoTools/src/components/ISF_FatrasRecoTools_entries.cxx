#include "ISF_FatrasRecoTools/ISF_SiSpacePointMakerTool.h"
#include "ISF_FatrasRecoTools/ISF_PRDGeneralTruthTrajectorySorterID.h"
#include "ISF_FatrasRecoTools/ISF_PRD_AssociationTool.h"
#include "../ISF_PRDtoTrackMapTool.h"
#include "ISF_FatrasRecoTools/ISF_TrackSummaryHelperTool.h"

using namespace iFatras;

DECLARE_COMPONENT( ISF_SiSpacePointMakerTool )
DECLARE_COMPONENT( ISF_PRDGeneralTruthTrajectorySorterID )
DECLARE_COMPONENT( ISF_PRD_AssociationTool )
DECLARE_COMPONENT( ISF_PRDtoTrackMapTool )
DECLARE_COMPONENT( ISF_TrackSummaryHelperTool )
